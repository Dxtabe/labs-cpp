#-------------------------------------------------
#
# Project created by QtCreator 2016-03-27T20:17:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DateDemo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    datetime.cpp

HEADERS  += mainwindow.h \
    datetime.h

FORMS    += mainwindow.ui

CONFIG   += c++11
