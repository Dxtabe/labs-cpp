#include "datetime.h"

using namespace std;

DateTime::DateTime(){
    Time = time(NULL);
}

DateTime::DateTime(int D, int M, int Y,
                   int h, int m, int s){
    struct tm sTime;
    sTime.tm_mday=D;
    sTime.tm_mon=M-1;
    sTime.tm_year=Y-1900;
    sTime.tm_hour=h+1;
    sTime.tm_min=m;
    sTime.tm_sec=s;
    Time=mktime(&sTime);
}

void DateTime::setDate(time_t t){
    if (t>0)
        Time=t;
    else
        Time=1;
}

void DateTime::setDate(){
    Time=time(NULL);
}

struct tm DateTime::getToday(){
    time_t Today;
    Today=time(NULL);
    return *localtime(&Today);
}

struct tm DateTime::getYesterday(){
    time_t Today;
    Today=time(NULL)-86400;
    return *localtime(&Today);
}

struct tm DateTime::getTomorrow(){
    time_t Today;
    Today=time(NULL)+86400;
    return *localtime(&Today);
}

struct tm DateTime::getFuture(int N)
{
    Time+=N*86400;
    return *localtime(&Time);
}

struct tm DateTime::getPast(int N){
    if(N<0)
        N=0;
    if (Time<N*86400)
        Time=1;
    else
        Time-=N*86400;
    return *localtime(&Time);
}

int DateTime::getMonth(){
    return localtime(&Time)->tm_mon;
}

int DateTime::getWeekDay(){
    return localtime(&Time)->tm_wday;
}

int DateTime::calcDifference(class DateTime D1,
                   class DateTime D2){
    return D1.Time>=D2.Time?
                (D1.Time-D2.Time)/86400:
                (D2.Time-D1.Time)/86400;
}
