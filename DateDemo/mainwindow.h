#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <ctime>
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbPast1_clicked();
    void on_pbPast2_clicked();
    void GetInfo1();
    void PrintInfo1(tm sTime);
    void GetInfo2();
    void PrintInfo2(tm sTime);

    void on_pbToday1_clicked();

    void on_pbFuture1_clicked();

    void on_pbToday2_clicked();

    void on_pbFuture2_clicked();

    void on_pbCalc_clicked();

    void on_pbGetWeekDay_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
