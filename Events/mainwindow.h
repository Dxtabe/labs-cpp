#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "event.h"
#include "mydelegate.h"
#include <QMainWindow>
#include <QStyle>
#include <QStandardItemModel>
#include <QFileDialog>
#include <QTimer>
#include <QXmlStreamReader>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    friend class MyDelegate;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QVector<Event> eventsList;
    void resizeEvent(QResizeEvent *event);
    void updateModel();
private slots:
    void timeout();
    void on_pbLoad_clicked();
};

#endif // MAINWINDOW_H
