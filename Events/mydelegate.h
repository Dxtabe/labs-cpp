#ifndef MYDELEGATE_H
#define MYDELEGATE_H
#include <QtWidgets>
#include <QApplication>
class MyDelegate : public QItemDelegate {
public:
    MyDelegate(QObject *pobj = 0) : QItemDelegate(pobj)
    {
    }
    void paint(QPainter* pPainter,
                           const QStyleOptionViewItem& option,
                           const QModelIndex& index
                           ) const
    {
        QStyleOptionProgressBar pbar;
        QStandardItemModel* m = (QStandardItemModel*) index.model();
        pbar.rect = option.rect;
        pbar.minimum = 0;
        pbar.maximum = 100;
        QDateTime s = QDateTime::fromString(m->item(index.row(), 2)->text());
        QDateTime e = QDateTime::fromString(m->item(index.row(), 3)->text());
        double startDiff = s.secsTo(e);
        double currDiff = QDateTime::currentDateTime().secsTo(e);
        double diff = 100.0 - ((100/(startDiff))*(currDiff));
        pbar.progress = diff > 100.0 || diff < 0.0 ? 100 : diff;
        pbar.text = QString::number(pbar.progress) + "%";
        pbar.textVisible = true;
        QApplication::style()->drawControl(QStyle::CE_ProgressBar, &pbar, pPainter);
    }
};

#endif // MYDELEGATE_H
