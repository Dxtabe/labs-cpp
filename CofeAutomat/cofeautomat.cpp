#include "cofeautomat.h"
#include <iostream>
#include <string>
#define CofN 8
using namespace std;

cofeautomat::cofeautomat()
{

}

int cofeautomat::on()
{
    if (state==OFF)
        state=WAIT;
    return state;
}

int cofeautomat::off()
{
    if (state==WAIT)
        state=OFF;
    return state;
}

string cofeautomat::printMenu()
{
    string buf;
    if (state==WAIT)
    {
        for(int i=0;i<9;i++)
        {
           buf+=menu[i];
           buf+=" ";
           buf+=to_string(prices[i]);
           if (i!=8)
               buf+=" ";
        }
    }
    return buf;
}

int cofeautomat::coin(int money)
{
    if (state==ACCEPT || state==WAIT)
    {
        state=ACCEPT;
        cash+=money;
    }
    return cash;
}

string cofeautomat::printState()
{
    string buf[5]={"OFF","WAIT","ACCEPT","CHECK","COOK"};
    return buf[state];
}

void cofeautomat::choice(int type)
{
    if (state==ACCEPT)
    {
        state=CHECK;
        choic=type;
    }
}

bool cofeautomat::check()
{
    if (state==CHECK)
    {
        if (cash>=prices[choic])
            chec=true;
        else
            chec=false;
    }
    return chec;
}

void cofeautomat::cancel()
{
    if (state==CHECK || state==ACCEPT)
    {
        state=WAIT;
        retcash=cash;
        cash=0;
    }
}

void cofeautomat::cook()
{
    if (state==CHECK || choic==true)
        state=COOK;
}

void cofeautomat::finish(int id)
{
    if (state==COOK)
    {
        retcash=cash-prices[id];
        cash=0;
        state=WAIT;
    }
}

int cofeautomat::getcoin()
{
    return cash;
}

int cofeautomat::getprice(int id)
{
    return prices[id];
}

int cofeautomat::getretcash()
{
    return retcash;
}
