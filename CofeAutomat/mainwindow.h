#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "cofeautomat.h"
#include <QPushButton>
#include <QTimer>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pb1r_clicked();

    void on_pb2r_clicked();

    void on_pb5r_clicked();

    void on_pb10r_clicked();

    void on_pb50r_clicked();

    void on_pb100r_clicked();

    void chandge_sum();

    void change_retsum();

    void pbact();

    void on_pbHW_clicked();

    void on_pbGT_clicked();

    void on_pbBT_clicked();

    void on_pbAmer_clicked();

    void on_pbEsp_clicked();

    void on_pbCap_clicked();

    void on_pbMac_clicked();

    void on_pbLat_clicked();

    void TimerTick();

    void AllButEnabled();

signals:
    void change_le_sum();

    void change_le_retsum();

    void All_But_Enabled();

    void pb_act();

private:
    Ui::MainWindow *ui;
    cofeautomat obj;
    QList <QPushButton*> widgets;
    QTimer *timer;
};

#endif // MAINWINDOW_H
