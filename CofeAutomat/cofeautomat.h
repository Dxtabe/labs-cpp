#ifndef COFEAUTOMAT_H
#define COFEAUTOMAT_H
#include <string>
#define CofN 8
using namespace std;


class cofeautomat
{
public:
    cofeautomat();
    int on();
    int off();
    int coin(int);
    string printState();
    string printMenu();
    void choice(int);
    bool check();
    void cancel();
    void cook();
    void finish(int);
    int getcoin();
    int getprice(int);
    int getretcash();
private:
    enum STATES{OFF,WAIT,ACCEPT,CHECK,COOK};
    int cash=0;
    int retcash=0;
    string menu[CofN]={"0.Hot water","1.Green tea","2.Black tea","3.Americano","4.Espresso","5.Cappucino","6.Macchiato","7.Latte"};
    int prices[CofN]={10,20,20,30,30,35,35,40};
    int state=OFF;
    int choic;
    bool chec;
};

#endif // COFEAUTOMAT_H
