#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QList>
#define CofN 8
#define AllBut 8+6

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->lReady->setVisible(false);
    obj.on();
    widgets.append(ui->pbHW);
    widgets.append(ui->pbGT);
    widgets.append(ui->pbBT);
    widgets.append(ui->pbAmer);
    widgets.append(ui->pbEsp);
    widgets.append(ui->pbCap);
    widgets.append(ui->pbMac);
    widgets.append(ui->pbLat);
    widgets.append(ui->pb1r);
    widgets.append(ui->pb2r);
    widgets.append(ui->pb5r);
    widgets.append(ui->pb10r);
    widgets.append(ui->pb50r);
    widgets.append(ui->pb100r);
    timer=new QTimer;
    connect(this,SIGNAL(change_le_sum()),this,SLOT(chandge_sum()));
    connect(this,SIGNAL(change_le_retsum()),this,SLOT(change_retsum()));
    connect(this,SIGNAL(pb_act()),this,SLOT(pbact()));
    connect(timer,SIGNAL(timeout()),this,SLOT(TimerTick()));
    connect(this,SIGNAL(All_But_Enabled()),this,SLOT(AllButEnabled()));
    timer->setInterval(100);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pb1r_clicked()
{
    obj.coin(1);
    emit change_le_sum();
    emit pb_act();
}

void MainWindow::on_pb2r_clicked()
{
    obj.coin(2);
    emit change_le_sum();
    emit pb_act();
}

void MainWindow::on_pb5r_clicked()
{
    obj.coin(5);
    emit change_le_sum();
    emit pb_act();
}

void MainWindow::on_pb10r_clicked()
{
    obj.coin(10);
    emit change_le_sum();
    emit pb_act();
}

void MainWindow::on_pb50r_clicked()
{
    obj.coin(50);
    emit change_le_sum();
    emit pb_act();
}

void MainWindow::on_pb100r_clicked()
{
    obj.coin(100);
    emit change_le_sum();
    emit pb_act();
}

void MainWindow::chandge_sum()
{
    QString str;
    str = QString::number(obj.getcoin());
    ui->leSum->setText(str);
}

void MainWindow::change_retsum()
{
    QString str;
    str = QString::number(obj.getretcash());
    ui->leRetSum->setText(str);
}

void MainWindow::pbact()
{

    for (int i=0;i<CofN;i++)
        if (obj.getcoin()>=obj.getprice(i))
            widgets[i]->setEnabled(true);
}

void MainWindow::on_pbHW_clicked()
{
    obj.choice(0);
    obj.check();
    obj.cook();
    obj.finish(0);
    ui->progBarCof->setValue(0);
    timer->start();
    emit All_But_Enabled();
}

void MainWindow::on_pbGT_clicked()
{
    obj.choice(1);
    obj.check();
    obj.cook();
    obj.finish(1);
    ui->progBarCof->setValue(0);
    timer->start();
    emit All_But_Enabled();
}

void MainWindow::on_pbBT_clicked()
{
    obj.choice(2);
    obj.check();
    obj.cook();
    obj.finish(2);
    ui->progBarCof->setValue(0);
    timer->start();
    emit All_But_Enabled();
}

void MainWindow::on_pbAmer_clicked()
{
    obj.choice(3);
    obj.check();
    obj.cook();
    obj.finish(3);
    ui->progBarCof->setValue(0);
    timer->start();
    emit All_But_Enabled();
}

void MainWindow::on_pbEsp_clicked()
{
    obj.choice(4);
    obj.check();
    obj.cook();
    obj.finish(4);
    ui->progBarCof->setValue(0);
    timer->start();
    emit All_But_Enabled();
}

void MainWindow::on_pbCap_clicked()
{
    obj.choice(5);
    obj.check();
    obj.cook();
    obj.finish(5);
    ui->progBarCof->setValue(0);
    timer->start();
    emit All_But_Enabled();
}

void MainWindow::on_pbMac_clicked()
{
    obj.choice(6);
    obj.check();
    obj.cook();
    obj.finish(6);
    ui->progBarCof->setValue(0);
    timer->start();
    emit All_But_Enabled();
}

void MainWindow::on_pbLat_clicked()
{
    obj.choice(7);
    obj.check();
    obj.cook();
    obj.finish(7);
    ui->progBarCof->setValue(0);
    timer->start();
    emit All_But_Enabled();
}

void MainWindow::TimerTick()
{
    ui->lReady->setVisible(false);
    ui->progBarCof->setValue(ui->progBarCof->value()+1);
    if(ui->progBarCof->value()==100)
    {
        ui->lReady->setVisible(true);
        emit change_le_retsum();
        emit change_le_sum();
    }
}

void MainWindow::AllButEnabled()
{
    for (int i=0;i<AllBut;i++)
            widgets[i]->setEnabled(false);
}
