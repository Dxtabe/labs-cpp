#-------------------------------------------------
#
# Project created by QtCreator 2016-04-05T18:35:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CofeAutomat
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cofeautomat.cpp

HEADERS  += mainwindow.h \
    cofeautomat.h

FORMS    += mainwindow.ui
