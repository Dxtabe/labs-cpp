#include "vlonginteger.h"

//------------Constructors------------------------------

VLongInt::VLongInt(){
    Number=char(0);
    plus=true;
}

VLongInt::VLongInt(std::string IN){
    int sign;
    bool fault=false;
    Number="";

    if(IN[0]=='-')
        plus=false;
    else
        plus=true;
    sign=int(!plus);
    if(IN.length()==sign)
        Number=char(0);
    for (int i=sign; i<IN.length(); i++){
        if(IN[i]<'0' || IN[i]>'9')
        {
            fault=true;
            break;
        }
    }
    if(!fault)
        for (int i=IN.length()-1; i>=sign; i--){
            Number+=IN[i]-'0';
        }
    RemoveZeros();
}

//--------------Public-----------------------------------

std::string VLongInt::GetValue() const{
    std::string Out="";
    if(!plus)
        Out+='-';
    for(int i=Number.length()-1; i>=0; i--)
        Out+=char(Number[i]+'0');
    return Out;
}

VLongInt& VLongInt::operator +=(const VLongInt & N1){
    if(N1.plus==plus){
        std::string num1;
        std::string num2;
        if(N1.GetValue().length()>=Number.length()){
            num1=N1.Number;
            num2=Number;
        }
        else
        {
            num1=Number;
            num2=N1.Number;
        }
        int i=0;
        for(; i<num2.length()-1; i++){
            num1[i]+=num2[i];
            if (num1[i]>9)
            {
                num1[i]-=10;
                num1[i+1]++;
            }
        }
        num1[i]+=num2[i];
        for(;i<num1.length()-1;i++){
            if(num1[i]>9)
            {
                num1[i]-=10;
                num1[i+1]++;
            }
        }
        if (num1[i]>9)
        {
            num1[i]-=10;
            num1=num1+char(1);
        }
        Number=num1;
        return *this;
    }
    else
    {
        VLongInt tmp=N1;
        if (plus){
            tmp.plus=plus;
            return *this-=tmp;
        }
        else
        {
            plus=true;
            tmp-=*this;
            return *this=tmp;
        }
    }
}

VLongInt VLongInt::operator+(const VLongInt& N1) const{
    VLongInt Resault=*this;
    return Resault+=N1;
}


VLongInt& VLongInt::operator -=(const VLongInt & N1){
     if(N1.plus==plus)
     {
         if(*this==N1){
             Number=char(0);
             plus=true;
             return *this;
         }
         if (*this>N1){
             Substraction(N1);
             return *this;
         }
         VLongInt tmp=N1;
         plus=!plus;
         tmp.Substraction(*this);
         Number=tmp.Number;
         return *this;
     }
     else
     {
         VLongInt tmp=N1;
         tmp.plus=!tmp.plus;
         return *this+=tmp;
     }
}

VLongInt VLongInt::operator-(const VLongInt & N1) const {
    VLongInt Resault=*this;
    return Resault -=N1;
}

VLongInt& VLongInt::operator *=(const VLongInt & N1){
    std::string num1=Number, num2=N1.Number;
    VLongInt Resault, tmp;
    for (int i=0; i<num1.length(); i++)
    {
        tmp.Number="";
        tmp.Number.append(i, char(0));
        int perenos=0;
        int res_j;
        for(int j=0; j<num2.length(); j++)
        {
            res_j=num1[i]*num2[j]+perenos;
            tmp.Number=tmp.Number+char((res_j)%10);
            perenos=char(res_j/10);
        }
        if(perenos)
            tmp.Number=tmp.Number+char(perenos);
        Resault+=tmp;
    }
    if (plus==N1.plus)
        Resault.plus=true;
    else
        Resault.plus=false;
    return *this=Resault;
}

VLongInt VLongInt::operator*(const VLongInt & N1) const{
    VLongInt Resault=*this;
    return Resault*=N1;
}

VLongInt& VLongInt::operator /=(const VLongInt & N1){
    VLongInt Devident, Resault, tmp, num1=N1;
    num1.plus=true;
    tmp=num1;
    Devident.Number="";
    Resault.Number="";
    int j=0;
    int i=Number.length()-1;
    while (i>=0)
    {
        if(Devident.Number[Devident.Number.length()-1]==char(0))
            Devident.Number="";
        while(i>=0 && Devident<num1)
        {
            Devident.Number=Number[i]+Devident.Number;
            Devident.RemoveZeros();
            i--;
            Resault.Number+=char(0);
            j++;
        }
        if(tmp<Devident || tmp==Devident)
        {
            while(tmp<Devident || tmp==Devident){
                Resault.Number[j-1]++;
                tmp+=num1;
            }
            tmp-=num1;
            Devident-=tmp;
            tmp=num1;
        }
    }
    if (plus==N1.plus)
        plus=true;
    else
        plus=false;
    Number="";
    for(int i=Resault.Number.length()-1; i>=0; i--)
        Number+=Resault.Number[i];
    RemoveZeros();
    return *this;
}

VLongInt VLongInt::operator/(const VLongInt& N1) const{
    VLongInt Resault=*this;
    return Resault/=N1;
}

VLongInt& VLongInt::operator %=(const VLongInt &N1){
    this->plus=true;
    VLongInt Devider=N1, Res=*this;
    Devider.plus=true;
    Res.plus=true;
    Res/=Devider;
    Res*=Devider;
    Res.RemoveZeros();
    return *this-=Res;
}

VLongInt VLongInt::operator%(const VLongInt& N1) const{
    VLongInt Resault=*this;
    return Resault%=N1;
}

//возведение в степень игнорирует отрицательные показатели
VLongInt& VLongInt::operator ^=(const VLongInt &N1){
    VLongInt tmp=*this;
    Number=char(1);
    bool sign=plus;
    std::string kofBin = To_Binary(N1); //степень к двоичному виду
    for(int i=0; i<kofBin.length(); i++){
        if(kofBin[i])
            *this*=tmp;
        if(i<kofBin.length()-1)
            tmp*=tmp;
    }
    if(kofBin[0])  //если нечетное, то смени степень
        plus=sign;
    else
        plus=true;
    return *this;
}

VLongInt VLongInt::operator^(const VLongInt& N1) const{
    VLongInt Resault=*this;
    return Resault^=N1;
}

bool VLongInt::operator >(const VLongInt & N1){
    if (plus>N1.plus)
        return true;
    if (plus<N1.plus)
        return false;
    if(Number.length()<N1.Number.length())
        return false;
    else if (Number.length()>N1.Number.length())
        return true;
    std::string num1=Number, num2=N1.Number;
    int i=num1.length()-1;
    while (i>=0){
        if(num1[i]>num2[i])
            return true;
        else if (num1[i]<num2[i])
            return false;
        else
            i--;
    }
    return false;
}

bool VLongInt::operator ==(const VLongInt & N1) {
    if (plus==N1.plus)
        return Number==N1.Number;
    else
        return false;
}

bool VLongInt::operator <(const VLongInt & N1){
    if(*this>N1 || *this == N1)
        return false;
    else
        return true;
}

//----------------------Private-------------------------------------

void VLongInt::Substraction(const VLongInt & N1){
    std::string num1=Number, num2=N1.Number;
    int i=0;
    bool perenos=false;
    for(;i<num2.length();i++){
        if(num1[i]<num2[i]+perenos)
        {
            num1[i]=num1[i]+10-num2[i]-perenos;
            perenos=true;
        }
        else
        {
            num1[i]-=char(num2[i]+perenos);
            perenos=false;
        }
    }
    for(;i<num1.length();i++){
        if(num1[i]<perenos)
            num1[i]=char(9);
        else
        {
            num1[i]-=perenos;
            perenos=false;
        }
    }
    Number=num1;
    RemoveZeros();
}

void VLongInt::RemoveZeros(){
    int i=Number.length()-1;
    while(!Number[i] && i>0){
        Number.erase(Number.end()-1);
        i--;
    }
}

std::string VLongInt::To_Binary(const VLongInt & N1){
    VLongInt In=N1;
    In.plus=true;
    VLongInt tmp=In;
    VLongInt Bin("2");
    std::string Out="";
    while(In>Bin || In==Bin)
    {
        In/=Bin;
        tmp%=Bin;
        Out+=tmp.Number;
        tmp=In;
    }
    Out+=In.Number;
    return Out;
}
