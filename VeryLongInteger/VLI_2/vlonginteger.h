#ifndef VLONGINTEGER_H
#define VLONGINTEGER_H

#include <string>

class VLongInt {
public:
    VLongInt (std::string);
    VLongInt();
    std::string GetValue() const;
    bool GetSign() const {return plus;}
    VLongInt &operator+=(const VLongInt&);
    VLongInt operator+(const VLongInt&) const;
    VLongInt &operator-=(const VLongInt&);
    VLongInt operator-(const VLongInt&) const;
    VLongInt &operator*=(const VLongInt&);
    VLongInt operator*(const VLongInt&) const;
    VLongInt &operator/=(const VLongInt&);
    VLongInt operator/(const VLongInt&) const;
    VLongInt &operator%=(const VLongInt&);
    VLongInt operator%(const VLongInt&) const;
    VLongInt &operator ^=(const VLongInt&);
    VLongInt operator^(const VLongInt&) const;
    bool operator>(const VLongInt&);
    bool operator ==(const VLongInt&);
    bool operator<(const VLongInt&);

private:
    std::string Number;
    bool plus;
    void Substraction(const VLongInt&);
    std::string To_Binary(const VLongInt&);
    void RemoveZeros();
};


#endif // VLONGINTEGER_H
