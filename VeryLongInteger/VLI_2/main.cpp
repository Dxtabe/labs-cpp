#include <iostream>
#include "vlonginteger.h"
#include <time.h>

using namespace std;

int main(int argc, char *argv[])
{
    VLongInt N1("20");
    VLongInt N2("20000");
    cout <<"N1="<<N1.GetValue()<<" N2=" << N2.GetValue()<<endl;
    clock_t start=clock(), end;
/*    N1+=N2;
    cout <<"+= "<< N1.GetValue() << endl;

    N1-=N2;
    cout <<"native "<<N1.GetValue()<<endl;

    N1-=N2;
    cout <<"-= "<<N1.GetValue()<<endl;

    N1*=N2;
    cout <<"*= "<<N1.GetValue()<<endl;

    cout <<"N1==N2 "<< int(N1==N2)<<endl;

    N1/=N2;
    cout <<"/= "<<N1.GetValue()<<endl;

    N1%=N2;
    cout <<"%="<<N1.GetValue()<<endl;

    N1^=N2;
    cout <<"^="<<N1.GetValue()<<endl;

    cout <<"N1*N2 "<<(N1*N2).GetValue()<<endl;
*/
    N1%=N2;
    end=clock();
    cout<<"N1%N2 = "<<N1.GetValue()<<endl;
    cout<<(float)(end-start)/CLOCKS_PER_SEC<<endl;
    return 0;
}
