TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    vlonginteger.cpp

HEADERS += \
    vlonginteger.h
