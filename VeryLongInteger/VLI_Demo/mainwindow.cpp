#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbDevide_clicked()
{
    QString in=ui->textEdit->toPlainText();
    VLongInt N1(in.toStdString());
    in=ui->textEdit_2->toPlainText();
    VLongInt N2(in.toStdString());
    N1/=N2;
    ui->textEdit->setText(N1.GetValue().c_str());
}

void MainWindow::on_pbPlus_clicked()
{
    QString in=ui->textEdit->toPlainText();
    VLongInt N1(in.toStdString());
    in=ui->textEdit_2->toPlainText();
    VLongInt N2(in.toStdString());
    N1+=N2;
    ui->textEdit->setText(N1.GetValue().c_str());
}

void MainWindow::on_pbMinus_clicked()
{
    QString in=ui->textEdit->toPlainText();
    VLongInt N1(in.toStdString());
    in=ui->textEdit_2->toPlainText();
    VLongInt N2(in.toStdString());
    N1-=N2;
    ui->textEdit->setText(N1.GetValue().c_str());
}

void MainWindow::on_pbMulty_clicked()
{
    QString in=ui->textEdit->toPlainText();
    VLongInt N1(in.toStdString());
    in=ui->textEdit_2->toPlainText();
    VLongInt N2(in.toStdString());
    N1*=N2;
    ui->textEdit->setText(N1.GetValue().c_str());
}

void MainWindow::on_pbRest_clicked()
{
    QString in=ui->textEdit->toPlainText();
    VLongInt N1(in.toStdString());
    in=ui->textEdit_2->toPlainText();
    VLongInt N2(in.toStdString());
    N1%=N2;
    ui->textEdit->setText(N1.GetValue().c_str());
}

void MainWindow::on_pbExp_clicked()
{
    QString in=ui->textEdit->toPlainText();
    VLongInt N1(in.toStdString());
    in=ui->textEdit_2->toPlainText();
    VLongInt N2(in.toStdString());
    N1^=N2;
    ui->textEdit->setText(N1.GetValue().c_str());
}
