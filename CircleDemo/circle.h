#ifndef CIRCLE_H
#define CIRCLE_H

#endif // CIRCLE_H

class Circle{
public:
    Circle();
    Circle(double);
    void setRadius(double);
    void setFerence(double);
    void setSquare(double);
    double GetRadius();
    double GetFerence();
    double GetSquare();
private:
    double radius;
    double ference;
    double square;

};
