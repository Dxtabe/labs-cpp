#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "circle.h"

Circle demo, Earth;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    on_actionDemo_triggered();
}

MainWindow::~MainWindow()
{
    delete ui;
}
//demo
void MainWindow::on_actionDemo_triggered()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_pbRadius_clicked()
{
    QString r=ui->leRadius->text();
    demo.setRadius(r.toDouble());
    ui->leFerence->setText(QString::number(demo.GetFerence()));
    ui->leSqure->setText(QString::number(demo.GetSquare()));
}

void MainWindow::on_pbSqure_clicked()
{
    QString r=ui->leSqure->text();
    demo.setSquare(r.toDouble());
    ui->leRadius->setText(QString::number(demo.GetRadius()));
    ui->leFerence->setText(QString::number(demo.GetFerence()));
}

void MainWindow::on_pbFerence_clicked()
{
    QString r=ui->leFerence->text();
    demo.setFerence(r.toDouble());
    ui->leRadius->setText(QString::number(demo.GetRadius()));
    ui->leSqure->setText(QString::number(demo.GetSquare()));
}

//Earth
void MainWindow::on_actionEarth_triggered()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::on_leLenth_cursorPositionChanged(int arg1, int arg2)
{
    QString inRadius=ui->lRadius->text();
    Earth.setRadius(inRadius.toDouble());
    QString inLenth=ui->leLenth->text();
    Earth.setFerence(Earth.GetFerence()+inLenth.toDouble());
    ui->lResaultEarth->setText(QString::number(Earth.GetRadius()-inRadius.toDouble()));
}

//Pool
void MainWindow::on_actionPool_triggered()
{
    ui->stackedWidget->setCurrentIndex(2);
}


void MainWindow::on_pbCalc_clicked()
{
    QString inConcretePrice=ui->leConcretePrice->text();
    QString inHedgePrice=ui->leHedgePrice->text();
    QString inPoolRadius=ui->lePoolRadius->text();
    QString inRoadWidth=ui->leRoadWidth->text();
    class Circle Pool(inPoolRadius.toDouble()),
            EntirePool(inPoolRadius.toDouble()+inRoadWidth.toDouble());
    ui->lResaultPool->setText(QString::number(
                       (EntirePool.GetSquare()-Pool.GetSquare())*inConcretePrice.toDouble()
                                  +EntirePool.GetFerence()*inHedgePrice.toDouble()));

}
