#include "add.h"
#include "ui_add.h"

Add::Add(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Add)
{
    ui->setupUi(this);
    QDialog::setWindowTitle("Add");
}

Add::~Add()
{
    delete ui;
}

void Add::on_buttonBox_accepted()
{
    std::string Nam, Num;
    Nam.clear();
    Num.clear();
    Nam=ui->leName->text().toStdString();
    if(Nam.empty())
        return;
    Num=ui->leNumber->text().toStdString();
    if(Num.empty())
        return;
    emit add_signal(Nam, Num);
    QDialog::close();
}
