#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
using namespace std;
#define SERVER_ADRESS "127.0.0.1"
#define SERVER_PORT 3425

char message[]="Hello there!\n";
char buf[1024];
int main()
{
    int sock;
    struct sockaddr_in addr;
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock<0)
    {
        perror("socket");
        exit(1);
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(SERVER_PORT);
    addr.sin_addr.s_addr = inet_addr(SERVER_ADRESS);
    if(connect(sock, (struct sockaddr*)&addr, sizeof(addr))<0)
    {
        perror("connect");
        exit(2);
    }
    else
        cout<<"Connection"<<endl;
    while(1)
    {
        recv(sock, buf, sizeof(buf), 0);
        printf(buf);
    }
    close(sock);
    return 0;
}
