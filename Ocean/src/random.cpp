#include "/home/arkady/Projects/Ocean/src/random.h"
#define MAX 32767

//методы доступа
float Random::randReal()
{
    int c;
    static int first=1;
    
    /*Этот код выполняется один раз*/
    if(first)
    {
        seed1*=2;
        seed2*=2;
    if(seed1>MAX)seed1-=MAX;
    if(seed2>MAX)seed2-=MAX;
    first=0;
    
    //возбуждение генератора
    for(int index=1;index<=30;index++)
        randReal();
    }
c=seed1+seed2;
if(c>MAX)c-=MAX;
c*=2;
if(c>MAX)c-=MAX;
seed1=seed2;
seed2=c;
return (float)c/32767.0;
}

unsigned int Random::nextIntBetween(int low,int high)
{
    float r,t;
    int c;
    
    r=(float)high-(float)low+1.0;
    t=r*randReal();
    c=(int)t;
    return (low+c);
}
