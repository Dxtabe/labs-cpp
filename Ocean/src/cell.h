#ifndef CELL_H
#define CELL_H

#include "/home/arkady/Projects/Ocean/src/ocean.h"

class Cell
{
    friend class Ocean;

protected:
    static Ocean *Ocean1;
    Coordinate offset;
    char image;

    //методы поиска соседей
    Cell* getCellAt(Coordinate aCoord);
    void assignCellAt(Coordinate aCoord, Cell* aCell);
    Cell* getNeighborWithImage(char animage);
    Coordinate getEmptyNeighborCoord();
    Coordinate getPreyNeighborCoord();
    Cell* north();
    Cell* south();
    Cell* east();
    Cell* west();
    Cell* northeast();
    Cell* northwest();
    Cell* southeast();
    Cell* southwest();

    //методы обработки и отображения
    virtual Cell* reproduce(Coordinate anOffset);

public:
    Cell(Coordinate& aCoord):offset(aCoord){image=DefaultImage;}
    Cell(){}
    virtual ~Cell(){}

    //методы доступа
    Coordinate& getOffset(){return offset;}
    void setOffset(Coordinate& anOffset){offset=anOffset;}
    char getImage(){return image;}

    //методы обработки и отображения
    void display();
    virtual void process(){}

    //метод сети
    void thread_net();
};

#endif // CELL_H
