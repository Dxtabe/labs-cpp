#include "/home/arkady/Projects/Ocean/src/ocean.h"
#include "/home/arkady/Projects/Ocean/src/stone.h"
#include "/home/arkady/Projects/Ocean/src/cell.h"
#include "/home/arkady/Projects/Ocean/src/constants.h"
#include "/home/arkady/Projects/Ocean/src/prey.h"
#include "/home/arkady/Projects/Ocean/src/predator.h"
#include "/home/arkady/Projects/Ocean/src/random.h"
#include <iostream>
#include <QThread>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <chrono>
#include <thread>
using namespace std;
#define SERVER_ADRESS "127.0.0.1"
#define SERVER_PORT 3425

bool isActive = false;
int age = 0;

void Cell::thread_net()
{
    int sock, listener;
    struct sockaddr_in addr;
    char buf[1024];
    FILE *fp;
    listener = socket(AF_INET, SOCK_STREAM, 0);
    if (listener<0)
    {
        perror("socket");
        exit(1);
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(SERVER_PORT);
    addr.sin_addr.s_addr = inet_addr(SERVER_ADRESS);
    if(bind(listener, (struct sockaddr*)&addr, sizeof(addr))<0)
    {
        perror("bind");
        exit(2);
    }
    listen(listener,1);
    while(1)
    {
        sock = accept(listener, NULL, NULL);
        if(sock<0)
        {
            perror("accept");
            exit(3);
        }
        cout<<"Accepted";
        while(isActive)
        {
            //fp = fopen("server.log", "a");
            //fprintf(fp, "%d\t%d\t%d\n", age, ocean->getNumPrey(), ocean->getNumPredators());
            sprintf(buf,"%d\t%d\t%d\n", age, Cell::Ocean1->getNumPrey(), Cell::Ocean1->getNumPredators());
            //fclose(fp);
            send(sock, buf, sizeof(buf), 0);
            cout<<"send alredy"<<endl;
            this_thread::sleep_until(chrono::system_clock::now()+chrono::seconds(1));
        }
        close(sock);
    }
}

//методы инициализации
void Ocean::initialize()
{
    random.initiallize();
    numRows=MaxRows;
    numCols=MaxCols;
    size=numRows*numCols;
    
    numStones=DefaultNumStones;
    numPrey=DefaultNumPrey;
    numPredators=DefaultNumPredators;
    
    initCells();
}

void Ocean::initCells()
{
    addEmptyCells();
    //ввод кол-ва камней
    cout<<"\n\nEnter number of stones(default=75):";
    cout.flush();
    cin>>numStones;
    if(numStones>size)
        numStones=size;
    cout<<"\nNumber stones accepted="<<numStones;
    cout.flush();
    //ввод кол-ва добычи
    cout<<"\n\nEnter number of prey(default=150):";
    cout.flush();
    cin>>numPrey;
    if(numPrey>(size-numStones))
        numPrey=(size-numStones);
    cout<<"\nNumber prey accepted="<<numPrey;
    cout.flush();
    //ввод кол-ва хищников
    cout<<"\n\nEnter number of predators(default=20):";
    cout.flush();
    cin>>numPredators;
    if(numPredators>(size-numStones-numPrey))
        numPrey=(size-numStones-numPrey);
    cout<<"\nNumber predators accepted="<<numPredators<<endl;
    cout.flush();
    
    addStones();
    addPrey();
    addPredators();
    displayBorder();
    displayCells();
    displayBorder();
    displayStats(-1);
    Cell::Ocean1=this;//прикрепить инициализированный океан ко всем ячейкам
}

void Ocean::addEmptyCells()
{
    for(unsigned int row=0;row<numRows;row++)
        for(unsigned int col=0;col<numCols;col++)
            cells[row][col]=new Cell(*new Coordinate(col,row));
}

void Ocean::addStones()
{
    Coordinate empty;
    for(unsigned int count=0;count<numStones;count++)
    {
        empty=getEmptyCellCoord();
        cells[empty.getY()][empty.getX()]=new Stone(empty);
    }
}

void Ocean::addPrey()
{
    Coordinate empty;
    for(unsigned int count=0;count<numPrey;count++)
    {
        empty=getEmptyCellCoord();
        cells[empty.getY()][empty.getX()]=new Prey(empty);
    }    
}

void Ocean::addPredators()
{
    Coordinate empty;
    for(unsigned int count=0;count<numPredators;count++)
    {
        empty=getEmptyCellCoord();
        cells[empty.getY()][empty.getX()]=new Predator(empty);
    }
}

Coordinate Ocean::getEmptyCellCoord()
{
    unsigned int x,y;
    Coordinate empty;
    do
    {
        x=random.nextIntBetween(0,numCols-1);
        y=random.nextIntBetween(0,numRows-1);
    }while(cells[y][x]->getImage()!=DefaultImage);
    empty=cells[y][x]->getOffset();
    delete cells[y][x];
    return empty;
}

//методы отображения
void Ocean::displayBorder()
{
    for(unsigned int col=0;col<numCols;col++)
        cout<<"*";
    cout<<"\n";
}

void Ocean::displayCells()
{
    for(unsigned int row=0;row<numRows;row++)
    {
        for(unsigned int col=0;col<numCols;col++)
            cells[row][col]->display();
        cout<<"\n";
    }
}

void Ocean::displayStats(int iteration)
{
    cout<<"\n\n";
    cout<<"Iteration number="<<++iteration<<", ";
    cout<<"Prey="<<numPrey<<", ";
    cout<<"Predators="<<numPredators<<", ";
    cout<<"Stones="<<numStones<<endl;
}

//метод запуска
void Ocean::run()
{
    //thread t1(Cell::thread_net);
    //t1.detach();
    unsigned int numIterations=DefaultNumIterations;
    //ввод кол-ва итераций
    cout<<"\n\nEnter number of iterations(default max=1000):";
    cout.flush();
    cin>>numIterations;
    if(numIterations>1000)
        numIterations=1000;
    cout<<"\nNumber iterations="<<numIterations<<"\nSimulation starting...\n";
    cout.flush();
    
    for(unsigned int iteration=0;iteration<numIterations;iteration++)
    {
        //system("clear");
        if(numPredators>0 && numPrey>0)
        {
            for(unsigned int row=0;row<numRows;row++)
                for(unsigned int col=0;col<numCols;col++)
                    cells[row][col]->process();
            displayBorder();
            displayCells();
            displayBorder();
            displayStats(iteration);
            cout.flush();
            isActive = true;
            QThread::msleep(300);
            //printf(fp, "%d\t%d\t%d\n", age, ocean->getNumPrey(), ocean->getNumPredators());
            //this_thread::sleep_until(chrono::system_clock::now()+chrono::seconds(1));
            age++;
        }
    }
    cout<<"\n\nEnd of simulation\n";
    cout.flush();
}
