#include "/home/arkady/Projects/Ocean/src/predator.h"

//методы обработки и отображения
void Predator::process()
{
    Coordinate toCoord;
    if(--timeToFeed<=0)//хищник умирает
    {
        assignCellAt(offset,new Cell(offset));
        Ocean1->setNumPredators(Ocean1->getNumPredators()-1);
        delete this;
    }
    else//съедает соседнюю добычу(если возможно)
    {
        toCoord=getPreyNeighborCoord();
        if(toCoord!=offset)
        {
            Ocean1->setNumPrey(Ocean1->getNumPrey()-1);
            timeToFeed=TimeToFeed;
            moveFrom(offset,toCoord);
        }
        else
        Prey::process();//перемещение в пустую ячейку(если возможно)
    }
}

Cell* Predator::reproduce(Coordinate anOffset)
{
    Predator* temp=new Predator(anOffset);
    Ocean1->setNumPredators(Ocean1->getNumPredators()+1);
    return (Cell*)temp;
}
