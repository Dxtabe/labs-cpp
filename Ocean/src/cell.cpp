#include "/home/arkady/Projects/Ocean/src/cell.h"
#include <iostream>
using namespace std;

Ocean* Cell::Ocean1=NULL;

//методы поиска соседей
Cell* Cell::getCellAt(Coordinate aCoord)
{
    return Ocean1->cells[aCoord.getY()][aCoord.getX()];
}

void Cell::assignCellAt(Coordinate aCoord,Cell* aCell)
{
    Ocean1->cells[aCoord.getY()][aCoord.getX()]=aCell;
}

Cell* Cell::getNeighborWithImage(char animage)
{
    Cell* neighbors[8];
    unsigned int count=0;
    
    if(north()->getImage()==animage)neighbors[count++]=north();
    if(south()->getImage()==animage)neighbors[count++]=south();
    if(east()->getImage()==animage)neighbors[count++]=east();
    if(west()->getImage()==animage)neighbors[count++]=west();
    if(southeast()->getImage()==animage)neighbors[count++]=southeast();
    if(southwest()->getImage()==animage)neighbors[count++]=southwest();
    if(northeast()->getImage()==animage)neighbors[count++]=northeast();
    if(northwest()->getImage()==animage)neighbors[count++]=northwest();
    if(count==0)
        return this;
    else
        return neighbors[Ocean1->random.nextIntBetween(0,count-1)];    
}

Coordinate Cell::getEmptyNeighborCoord()
{
    return getNeighborWithImage(DefaultImage)->getOffset();
}

Coordinate Cell::getPreyNeighborCoord()
{
    return getNeighborWithImage(DefaultPreyImage)->getOffset();
}

Cell* Cell::north()
{
    unsigned int yvalue;
    yvalue=(offset.getY()>0?(offset.getY()-1):(Ocean1->numRows-1));
    return Ocean1->cells[yvalue][offset.getX()];
}

Cell* Cell::south()
{
    unsigned int yvalue;
    yvalue=(offset.getY()+1)%Ocean1->numRows;
    return Ocean1->cells[yvalue][offset.getX()];
}

Cell* Cell::east()
{
    unsigned int xvalue;
    xvalue=(offset.getX()+1)%Ocean1->numCols;
    return Ocean1->cells[offset.getY()][xvalue];
}

Cell* Cell::west()
{
    unsigned int xvalue;
    xvalue=(offset.getX()>0?(offset.getX()-1):(Ocean1->numCols-1));
    return Ocean1->cells[offset.getY()][xvalue];
}

Cell* Cell::northeast()
{
    unsigned int xvalue, yvalue;
    xvalue=(offset.getX()+1)%Ocean1->numCols;
    yvalue=(offset.getY()>0?(offset.getY()-1):(Ocean1->numRows-1));
    return Ocean1->cells[yvalue][xvalue];
}

Cell* Cell::northwest()
{
    unsigned int xvalue, yvalue;
    xvalue=(offset.getX()>0?(offset.getX()-1):(Ocean1->numCols-1));
    yvalue=(offset.getY()>0?(offset.getY()-1):(Ocean1->numRows-1));
    return Ocean1->cells[yvalue][xvalue];
}

Cell* Cell::southeast()
{
    unsigned int xvalue, yvalue;
    xvalue=(offset.getX()+1)%Ocean1->numCols;
    yvalue=(offset.getY()+1)%Ocean1->numRows;
    return Ocean1->cells[yvalue][xvalue];
}

Cell* Cell::southwest()
{
    unsigned int xvalue, yvalue;
    xvalue=(offset.getX()>0?(offset.getX()-1):(Ocean1->numCols-1));
    yvalue=(offset.getY()+1)%Ocean1->numRows;
    return Ocean1->cells[yvalue][xvalue];
}

//методы обработки и отображения
Cell* Cell::reproduce(Coordinate anOffset)
{
    Cell* temp=new Cell(anOffset);
    return temp;
}

void Cell::display()
{
    cout<<image;
}
