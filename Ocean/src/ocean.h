#ifndef OCEAN_H
#define OCEAN_H

#include "/home/arkady/Projects/Ocean/src/constants.h"
#include "/home/arkady/Projects/Ocean/src/random.h"
#include "/home/arkady/Projects/Ocean/src/coordinate.h"

class Cell;

class Ocean
{
    friend class Cell;

public:
    //методы доступа
    unsigned int getNumPrey() const {return numPrey;}
    unsigned int getNumPredators() const {return numPredators;}
    void setNumPrey(unsigned int aNumber){numPrey=aNumber;}
    void setNumPredators(unsigned int aNumber){numPredators=aNumber;}

    //метод инициализации
    void initialize();

    //метод запуска
    void run();

private:
    unsigned int numRows;
    unsigned int numCols;
    unsigned int size; //numRows*numCols
    unsigned int numPrey;
    unsigned int numPredators;
    unsigned int numStones;
    Random random;
    Cell* cells[MaxRows][MaxCols];

    //методы инициализации
    void initCells();
    void addEmptyCells();
    void addStones();
    void addPrey();
    void addPredators();
    Coordinate getEmptyCellCoord();

    //методы отображения
    void displayBorder();
    void displayCells();
    void displayStats(int iteration);
};

#endif // OCEAN_H
