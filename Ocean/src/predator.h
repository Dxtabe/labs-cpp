#ifndef PREDATOR_H
#define PREDATOR_H

#include "/home/arkady/Projects/Ocean/src/prey.h"

class Predator:public Prey
{
private:
    //методы обработки и отображения
    virtual Cell* reproduce(Coordinate anOffset);

protected:
    unsigned int timeToFeed;

public:
    Predator(Coordinate aCoord):Prey(aCoord)
    {
        timeToFeed=TimeToFeed;
        image=DefaultPredImage;
    }
    virtual ~Predator(){}

    //методы обработки и отображения
    virtual void process();
};

#endif // PREDATOR_H
