#ifndef RANDOM_H
#define RANDOM_H

class Random
{
private:
    int seed1,seed2;

public:
    //методы инициализации
    void initiallize(){seed1=3797; seed2=2117;}
    void init(int s1, int s2){seed1=s1; seed2=s2;}

    //методы доступа
    float randReal();
    unsigned int nextIntBetween(int low, int high);
};

#endif // RANDOM_H
