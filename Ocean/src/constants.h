//максимальное значение по умолчанию для парамметров океана

#define MaxRows 25
#define MaxCols 70

#define DefaultNumStones 75
#define DefaultNumPrey 150
#define DefaultNumPredators 20
#define DefaultNumIterations 1000

#define DefaultImage '-'
#define DefaultPreyImage 'f'
#define DefaultPredImage 'S'
#define DefaultStonesImage '#'

#define TimeToFeed 6
#define TimeToReproduce 6
