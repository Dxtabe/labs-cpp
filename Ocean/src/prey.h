#ifndef PREY_H
#define PREY_H

#include "/home/arkady/Projects/Ocean/src/cell.h"

class Prey:public Cell
{
protected:
    int timeToReproduce;

    //методы обработки и отображения
    void moveFrom(Coordinate from, Coordinate to);
    virtual Cell* reproduce(Coordinate anOffset);

public:
    Prey(Coordinate& aCoord):Cell(aCoord)
    {
        timeToReproduce=TimeToReproduce;
        image=DefaultPreyImage;
    }
    virtual ~Prey(){}

    //методы обработки и отображения
    virtual void process()
    {
        Coordinate toCoord;
        toCoord=getEmptyNeighborCoord();
        moveFrom(offset,toCoord);
    }
};

#endif // PREY_H
