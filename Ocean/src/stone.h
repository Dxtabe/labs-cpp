#ifndef STRONE_H
#define STRONE_H

#include "/home/arkady/Projects/Ocean/src/predator.h"

class Stone:public Cell
{
public:
    Stone(Coordinate& aCoord):Cell(aCoord)
    {
        image=DefaultStonesImage;
    }

    virtual ~Stone(){}
};

#endif // STRONE_H
