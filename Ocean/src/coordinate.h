#ifndef COORDINATE_H
#define COORDINATE_H

class Coordinate
{
private:
    unsigned int x;
    unsigned int y;

public:
    Coordinate(unsigned int aX, unsigned int aY):x(aX),y(aY){}
    Coordinate(){x=0;y=0;}
    Coordinate(Coordinate& aCoord)
    {
        x=aCoord.x;
        y=aCoord.y;
    }
    ~Coordinate(){}

    //методы доступа
    unsigned int getX(){return x;}
    unsigned int getY(){return y;}
    void setX(unsigned int aX){x=aX;}
    void setY(unsigned int aY){y=aY;}

    //методы присвоения и сравнения
    //void operator = (Coordinate& aCoord)
    //{
      //  x=aCoord.x;
        //y=aCoord.y;
    //}

    int operator == (Coordinate& c)
    {
        return(x==c.x && y==c.y);
    }

    int operator != (Coordinate& c)
    {
        return(x!=c.x || y!=c.y);
    }
};

#endif // COORDINATE_H
