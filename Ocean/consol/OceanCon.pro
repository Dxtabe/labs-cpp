QT += core
QT -= gui

CONFIG += c++11

TARGET = OceanCon
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    ../Ocean/src/cell.cpp \
    ../Ocean/src/ocean.cpp \
    ../Ocean/src/predator.cpp \
    ../Ocean/src/prey.cpp \
    ../Ocean/src/random.cpp

HEADERS += \
    ../Ocean/src/cell.h \
    ../Ocean/src/constants.h \
    ../Ocean/src/coordinate.h \
    ../Ocean/src/ocean.h \
    ../Ocean/src/predator.h \
    ../Ocean/src/prey.h \
    ../Ocean/src/random.h \
    ../Ocean/src/stone.h
